module ApplicationHelper
  def full_title(title: "")
    if title.blank?
      BASE_TITLE
    else
      "#{title} - #{BASE_TITLE}"
    end
  end

  def add_active_to_correct_item(i)
    if i == 0
      " active"
    end
  end

  # 不適切なsizeを設定すると例外発生（この関数がimage_tagの引数になるとき）
  # コードのミスに気づきやすくする
  def describe_url(image, size_type: :product)
    return nil if [:mini, :small, :product, :large, nil].exclude?(size_type)
    return image.attachment(size_type) if image.present?
    "home/featured-collection/featured-collection-01.jpg"
  end

  # box_typeの値が間違うと例外発生
  def width_of_productBox(box_type)
    case box_type
    when "relation"
      "col-md-3 col-sm-6 col-xs-12"
    when "category"
      "col-sm-4 col-xs-12"
    else
      raise "Value of 'box_type' is wrong! You are making a typo."
    end
  end
end
