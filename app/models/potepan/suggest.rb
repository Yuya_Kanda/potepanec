class Potepan::Suggest < ActiveRecord::Base
  def self.search(keyword, limit_arg)
    condition = "keyword like ?", "#{keyword}%"
    Potepan::Suggest.where(condition).limit(limit_arg).pluck(:keyword)
  end
end
