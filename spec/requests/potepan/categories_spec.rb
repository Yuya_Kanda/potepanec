require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :request do
  describe "GET /potepan/categories/:id" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon1) { create(:taxon, name: "taxon1", taxonomy: taxonomy, parent: taxonomy.root) }
    let(:taxon2) { create(:taxon, name: "taxon2", taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:product) { create(:product_with_images, price: 14.00, taxons: [taxon1]) }
    let!(:other_product) { create(:product_with_images, price: 15.00, taxons: [taxon2]) }

    before do
      get potepan_category_path(taxon1.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "include shop" do
      expect(response.body).to include "show_shop"
    end

    it "include only product.name" do
      expect(response.body).to include product.name
      expect(response.body).not_to include other_product.name
    end

    it "include only product.images" do
      expect(response.body).to include product.images.first.attachment(:product)
      expect(response.body).not_to include other_product.images.first.attachment(:product)
    end

    it "include product.price" do
      expect(response.body).to include product.display_price.to_s
      expect(response.body).not_to include other_product.display_price.to_s
    end

    it "include taxonomy at category_bar" do
      expect(response.body).to include taxonomy.name
    end

    it "include taxons at category_bar" do
      expect(response.body).to include taxon1.name
      expect(response.body).to include taxon2.name
    end
  end
end
