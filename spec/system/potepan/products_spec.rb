require 'rails_helper'

RSpec.describe 'Products', type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product) { create(:product_with_images, taxons: [taxon]) }
  let!(:relational_product) { create(:product_with_images, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "have items at correct position" do
    within(:css, '.media-body') do
      expect(page).to have_content(product.name)
    end
    within(:css, '#productBox-0') do
      expect(page).to have_content(relational_product.name)
    end
  end

  it "goes to category_path with click_on" do
    within(:css, '.media-body') do
      click_on "一覧ページへ戻る"
    end
    expect(page).to have_current_path potepan_category_path(taxon.id)
  end

  it "goes to product_path(relational_product.id) with click_on" do
    within(:css, '#productBox-0') do
      click_on relational_product.name
    end
    expect(page).to have_current_path potepan_product_path(relational_product.id)
  end

  # これより下記のテストは共通化されているので、このファイルのみ記述
  # spec/system/potepan/categories_spec.rbではhomeへのリンクをテストしない
  it "goes to home with click_on header_logo" do
    within(:css, ".header") do
      click_on "header_logo"
    end
    expect(page).to have_current_path potepan_path
  end

  it "goes to home with click_on header_Home" do
    within(:css, ".header") do
      click_on "Home"
    end
    expect(page).to have_current_path potepan_path
  end

  it "goes to home with click_on top_bar_Home" do
    within(:css, ".pageHeader") do
      click_on "Home"
    end
    expect(page).to have_current_path potepan_path
  end
end
