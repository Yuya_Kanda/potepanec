require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  describe "GET /potepan/products/:id" do
    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:taxon3) { create(:taxon) }
    let(:product) { create(:product_with_images, taxons: [taxon1, taxon2], price: 10.00) }
    let!(:relational_product1) { create(:product_with_images, taxons: [taxon1], price: 11.00) }
    let!(:no_relational_product) { create(:product_with_images, taxons: [taxon3], price: 12.00) }

    before do
      get potepan_product_path(product.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "not include shop" do
      expect(response.body).not_to include "show_shop"
    end

    it "include product" do
      expect(response.body).to include product.name
      expect(response.body).to include product.images.first.attachment(:large)
      expect(response.body).to include product.description
      expect(response.body).to include product.display_price.to_s
    end

    it "include relational_product" do
      expect(response.body).to include relational_product1.name
      expect(response.body).to include relational_product1.images.first.attachment(:product)
      expect(response.body).to include relational_product1.display_price.to_s
    end

    it "not include no_relational_product" do
      expect(response.body).not_to include no_relational_product.name
      expect(response.body).not_to include no_relational_product.images.first.attachment(:product)
      expect(response.body).not_to include no_relational_product.display_price.to_s
    end
  end
end
