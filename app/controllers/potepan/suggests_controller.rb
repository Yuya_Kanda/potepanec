class Potepan::SuggestsController < ApplicationController
  def index
    keyword = params[:keyword]
    uri = ENV["PRESITE_BASE_URI"]
    query = { keyword: keyword, max_num: MAX_SEARCH_CANDIDATES }
    header = { Authorization: "Bearer #{ENV["PRESITE_API_KEY"]}" }
    res = HTTPClient.get(uri, query, header)
    if res.status == 200
      render json: res.body
    else
      render json: { message: res.reason }, status: res.status
    end
  end
end
