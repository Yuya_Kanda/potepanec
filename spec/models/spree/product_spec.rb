require 'rails_helper'

RSpec.describe Spree::Product do
  describe "relational_products" do
    subject { product.relational_products }

    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:taxon3) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:relational_product1) { create(:product, taxons: [taxon1, taxon2]) }

    context "when have data of relational_product and no_relational_product" do
      let!(:no_relational_product) { create(:product, taxons: [taxon3]) }

      it { is_expected.to contain_exactly relational_product1 }
      it { is_expected.not_to include product }
      it { is_expected.not_to include no_relational_product }
    end

    context "when have data of 5 relational_products" do
      let!(:relational_product2) { create(:product, taxons: [taxon1, taxon3]) }
      let!(:relational_product3) { create(:product, taxons: [taxon2, taxon3]) }
      let!(:relational_product4) { create(:product, taxons: [taxon2]) }
      let!(:relational_product5) { create(:product, taxons: [taxon1]) }

      it "have up to 4 products" do
        is_expected.to contain_exactly(relational_product1,
                                       relational_product2,
                                       relational_product3,
                                       relational_product5)
      end
    end
  end
end
