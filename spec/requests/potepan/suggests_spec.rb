require 'rails_helper'

RSpec.describe Potepan::SuggestsController, type: :request do
  describe "search" do
    before do
      uri = ENV["PRESITE_BASE_URI"]
      query = { keyword: keyword, max_num: MAX_SEARCH_CANDIDATES }
      WebMock.enable!
      WebMock.stub_request(:get, uri).with(query: query).to_return(
        status: status,
        body: body
      )
      get potepan_suggests_path(keyword: keyword)
    end

    context "when error occurs with webapi" do
      let(:keyword) { "bag" }
      let(:status) { [404, "Not Found"] }
      let(:body) { "<h1>The page you were looking for doesn't exist.</h1>" }

      it "returns http error" do
        expect(response.status).to eq(404)
      end

      it "include Not Found" do
        expect(response.body).to include "Not Found"
      end
    end

    context "when keyword is bag" do
      let(:keyword) { "bag" }
      let(:status) { [200, "OK"] }
      let(:body) { "#{["bag", "bag for women", "bag for men"]}" }

      it "returns http success" do
        expect(response.status).to eq(200)
      end

      it "include bag for women" do
        expect(response.body).to include "bag for women"
      end

      it "not include apache" do
        expect(response.body).not_to include "apache"
      end
    end
  end
end
