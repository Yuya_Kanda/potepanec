module Potepanec::ProductDecorator
  def relational_products
    Spree::Product.in_taxons(taxons).distinct.
      having("id != ?", id).limit(MAXIMUM_NUMBER).includes(master: [:images, :default_price])
  end

  Spree::Product.prepend self
end
