class Potepan::ApiController < ApplicationController
  before_action :authenticate

  protected

  def authenticate
    authenticate_token || render_errors(:unauthorized, "Unauthorized")
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      token == ENV["SUGGEST_API_KEY"]
    end
  end

  def render_errors(status, message)
    obj = { message: message }
    render json: obj, status: status
  end
end
