require 'rails_helper'

RSpec.describe 'Suggests', type: :system do
  before do
    visit potepan_path
    uri = ENV["PRESITE_BASE_URI"]
    query = { keyword: keyword, max_num: MAX_SEARCH_CANDIDATES }
    WebMock.enable!
    WebMock.stub_request(:get, uri).with(query: query).to_return(
      status: status,
      body: body
    )
  end

  def hover_searchbox
    find('#searchBox').hover
    within(:css, '#searchBox') do
      yield
    end
  end

  after do
    WebMock.disable!
  end

  context "when error occurs controller at controller" do
    let(:keyword) { "bag" }
    let(:status) { [404, "Not Found"] }
    let(:body) { "<h1>The page you were looking for doesn't exist.</h1>" }

    it "show alert about server error", js: true do
      hover_searchbox do
        page.accept_confirm("404: Not Found") do
          fill_in('search', with: keyword)
        end
      end
    end
  end

  context "when fill in bag" do
    let(:keyword) { "bag" }
    let(:status) { [200, "OK"] }
    let(:body) { "#{["bag", "bag for women", "bag for men"]}" }

    it "show correct list", js: true do
      hover_searchbox do
        fill_in('search', with: keyword)
        expect(page).to have_content("bag")
        expect(page).to have_content("bag for women")
        expect(page).to have_content("bag for men")
        # 検索候補(bag for women)をクリックすると、検索ボックスにbag for womenが入力される
        find('li', text: 'bag for women').click
        expect(page).to have_content("検索")
        expect(find('#search').value).to eq("bag for women")
        expect(page).not_to have_content("bag")
      end
    end
  end

  context "when fill in ' '" do
    let(:keyword) { " " }

    it "show alert for only blank", js: true do
      hover_searchbox do
        page.accept_confirm("Please do not enter only blanks") do
          fill_in('search', with: keyword)
        end
      end
    end
  end
end
