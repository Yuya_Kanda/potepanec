class Potepan::Api::SuggestsController < Potepan::ApiController
  def index
    keyword = params[:keyword]
    render_errors(:bad_request, "keyword is blank") and return if keyword.blank?
    limit_arg = params[:max_num].to_i.positive? ? params[:max_num].to_i : nil
    suggests = Potepan::Suggest.search(keyword, limit_arg)
    render json: suggests
  end
end
