require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon1) { create(:taxon, name: "taxon1", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:taxon2) { create(:taxon, name: "taxon2", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product1) { create(:product_with_images, price: 14.00, taxons: [taxon1]) }
  let!(:product2) { create(:product_with_images, price: 15.00, taxons: [taxon2]) }

  before do
    visit potepan_category_path(taxon1.id)
  end

  it "have items at correct position" do
    within(:css, '#productBox-0') do
      expect(page).to have_content(product1.name)
    end
  end

  it "gose to category_path(taxon2.id) with click_on" do
    within(:css, '.sideBar') do
      click_on taxon2.name
    end
    expect(page).to have_current_path potepan_category_path(taxon2.id)
  end

  it "gose to product_path with click_on" do
    within(:css, '#productBox-0') do
      click_on product1.name
    end
    expect(page).to have_current_path potepan_product_path(product1.id)
  end
end
