require 'rails_helper'

RSpec.describe Potepan::Api::SuggestsController, type: :request do
  describe "index" do
    context "when unauthorized" do
      let(:query) { { keyword: "b", max_num: "2" } }
      let(:headers) { { Authorization: "Bearer invalid_api" } }

      before do
        get potepan_api_suggests_path, params: query, headers: headers
      end

      it { expect(response.status).to eq(401) }
    end

    context "when authorized" do
      let(:headers) { { Authorization: "Bearer #{ENV["SUGGEST_API_KEY"]}" } }
      let!(:bag) { create(:potepan_suggest, keyword: "bag") }
      let!(:bag_for_women) { create(:potepan_suggest, keyword: "bag for women") }
      let!(:bag_for_men) { create(:potepan_suggest, keyword: "bag for men") }

      before do
        get potepan_api_suggests_path, params: query, headers: headers
      end

      context "when keyword is nil" do
        let(:query) { { max_num: "2" } }

        it { expect(response.status).to eq(400) }
      end

      context "when keyword is blank" do
        let(:query) { { keyword: "", max_num: "2" } }

        it { expect(response.status).to eq(400) }
      end

      context "when keyword is 'b'" do
        context "when max_num is 2" do
          let(:query) { { keyword: "b", max_num: "2" } }

          it { expect(response.status).to eq(200) }
          it "include correct items" do
            expect(response.body).to include "bag"
            expect(response.body).to include "bag for women"
            expect(response.body).not_to include "bag for men"
          end
        end

        context "when max_num is 0" do
          let(:query) { { keyword: "b", max_num: "0" } }

          it { expect(response.status).to eq(200) }
          it "include correct items" do
            expect(response.body).to include "bag"
            expect(response.body).to include "bag for women"
            expect(response.body).to include "bag for men"
          end
        end

        context "when max_num is nil" do
          let(:query) { { keyword: "b" } }

          it { expect(response.status).to eq(200) }
          it "include correct items" do
            expect(response.body).to include "bag"
            expect(response.body).to include "bag for women"
            expect(response.body).to include "bag for men"
          end
        end
      end
    end
  end
end
