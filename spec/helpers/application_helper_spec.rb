require 'rails_helper'

# rubocop:disable EmptyLineAfterSubject, EmptyLineAfterFinalLet
RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    subject { full_title(title: title) }

    context "when arg is nothing" do
      subject { full_title }
      it { is_expected.to eq(BASE_TITLE) }
    end

    context "when arg = empty-string" do
      let(:title) { "" }
      it { is_expected.to eq(BASE_TITLE) }
    end

    context "when arg = nil" do
      let(:title) { nil }
      it { is_expected.to eq(BASE_TITLE) }
    end

    context "when arg = Product" do
      let(:title) { "Product" }
      it { is_expected.to eq("Product - #{BASE_TITLE}") }
    end
  end

  describe "add_active_to_correct_item" do
    subject { add_active_to_correct_item(n) }

    context "when arg = 0" do
      let(:n) { 0 }
      it { is_expected.to eq(" active") }
    end

    context "when arg = 2" do
      let(:n) { 2 }
      it { is_expected.to be_nil }
    end

    context "when arg = nil" do
      let(:n) { nil }
      it { is_expected.to be_nil }
    end
  end

  describe "describe_url" do
    subject { describe_url(image, size_type: size_type) }

    let(:image) { create(:image) }

    context "when (image)" do
      subject { describe_url(image) }
      it { is_expected.to eq(image.attachment(:product)) }
    end

    context "when (image, size_type: nil)" do
      let(:size_type) { nil }
      it { is_expected.to eq(image.attachment(:product)) }
    end

    context "when (image, size_type: :large)" do
      let(:size_type) { :large }
      it { is_expected.to eq(image.attachment(:large)) }
    end

    context "when (nil)" do
      subject { describe_url(nil) }
      it { is_expected.to eq("home/featured-collection/featured-collection-01.jpg") }
    end

    # 下記２つのケースはコードの打ち間違えをテストするもの
    context "when (image, size_type: :wrong)" do
      let(:size_type) { :wrong }
      it { is_expected.to be_nil }
    end

    context "when (nil, size_type: :wrong)" do
      subject { describe_url(nil, size_type: :wrong) }
      it { is_expected.to be_nil }
    end
  end

  # ２つのテスト以外はタイポなので、例外で処理
  describe "width_of_productBox" do
    subject { width_of_productBox(box_type) }

    context "when arg = relation" do
      let(:box_type) { "relation" }
      it { is_expected.to eq("col-md-3 col-sm-6 col-xs-12") }
    end

    context "when arg = category" do
      let(:box_type) { "category" }
      it { is_expected.to eq("col-sm-4 col-xs-12") }
    end
  end
end
# rubocop:enable EmptyLineAfterSubject, EmptyLineAfterFinalLet
